
package controller;

import java.util.LinkedHashMap;
import java.util.Map;
import model.Persona;
import model.PersonaValidar;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

public class formController {
    
    /*@RequestMapping(value="form.htm", method = RequestMethod.GET)
    public ModelAndView form (){
        return new ModelAndView("form","persona",new Persona());
    }*/
    
    private PersonaValidar personaValidar;
    public formController()
    {
        this.personaValidar=new PersonaValidar();
    }
    
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView form (){
        ModelAndView mav = new ModelAndView();
        mav.setViewName("form");
        mav.addObject("persona", new Persona());
        return mav;
    }
    
    @RequestMapping(value="form.htm", method=RequestMethod.POST)
    public String form(Persona per, ModelMap model)
    {
        model.addAttribute("nombre", per.getNombre());
        model.addAttribute("correo", per.getCorreo());
        model.addAttribute("edad", per.getEdad());
        model.addAttribute("pais", per.getPais());
        return "ok";
    }
    
    //Metodo para poblar select
    @ModelAttribute("paisLista")
    public Map<String, String> listadoPaises(){
        Map<String,String> pais=new LinkedHashMap<>();
        pais.put("Chile","Chile");
        pais.put("Mexico","Mexico");
        pais.put("Colombia","Colombia");
        pais.put("Argentina","Argentina");
        pais.put("Canada","Canada");
        return pais;
    }
    
    //Recibimos y validamos datos del formulario
    @RequestMapping(method=RequestMethod.POST)
    public ModelAndView form
        (
                @ModelAttribute("persona") Persona p,
                BindingResult result,
                SessionStatus status
        )
    {
        this.personaValidar.validate(p, result);
        if(result.hasErrors())
        {
            ModelAndView mav = new ModelAndView();
            mav.setViewName("form");
            mav.addObject("persona", new Persona());
            return mav;
        }else
        {
            ModelAndView mav = new ModelAndView();
            mav.setViewName("exito");
            mav.addObject("nombre",p.getNombre());
            mav.addObject("correo",p.getCorreo());
            mav.addObject("edad",p.getNombre());
            mav.addObject("pais",p.getPais());
            return mav;
        }
    }
}
