package model;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class PersonaValidar implements Validator{
    
    public boolean support(Class<?> type)
    {
        return Persona.class.isAssignableFrom(type);
    }
    
    @Override
    public void validate(Object o, Errors errors){
        Persona persona=(Persona)o;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nombre", "required.nombre", "El campo Nombre es Obligatorio");
    }

    @Override
    public boolean supports(Class<?> type) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
