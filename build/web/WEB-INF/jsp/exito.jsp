<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Se guardo correctamente</title>
    </head>
    <body>
        <h1>Resultado de los datos</h1>
        <ul>
            <li>Nombre: <c:out value="${nombre}" /></li>
            <li>Correo: <c:out value="${correo}" /></li>
            <li>Edad <c:out value="${edad}" /></li>
            <li>Pais: <c:out value="${pais}" /></li>
        </ul>
    </body>
</html>
