<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    </head>
    <body>
        <h1>Formulario</h1>
        <div class="container">
            <div class="row">
                <form:form method="post" commandName="persona" >
                    <p>
                        <form:label path="nombre">Nombre</form:label>
                        <form:input path="nombre" cssClass="form-control" />
                    </p>
                    <p>
                        <form:label path="correo">Correo</form:label>
                        <form:input path="correo" cssClass="form-control" />
                    </p>
                    <p>
                        <form:label path="edad">Edad</form:label>
                        <form:input path="edad" cssClass="form-control" />
                    </p>
                    <p>
                        <form:label path="pais">Pais</form:label>
                        <form:select path="pais" cssClass="form-control">
                            <form:option value="0" >Seleccione...</form:option>
                            <form:options items="${paisLista}" />
                        </form:select>
                    </p>
                    <hr>
                    <form:button>Enviar</form:button>
                </form:form>
            </div>
        </div>
    </body>
</html>
